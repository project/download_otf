<?php

namespace Drupal\download_otf\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines form for file extensions settings.
 */
class DownloadOtfConfig extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_otf_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->configFactory()->get('download_otf.settings')->get();

    $markup = $this->t('Manage file extension settings to download them On-The-Fly.');
    $form['caption'] = ['#markup' => '<p><strong>' . $markup . '</strong></p>'];

    // Header table.
    $form['header_list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('File Extension'),
        $this->t('Default Filename'),
        $this->t('Edit'),
        $this->t('Delete'),
      ],
      '#empty' => $this->t('There are no settings yet. Click <em>Add New</em> to create one.'),
    ];

    // Header table items.
    foreach ($settings as $id => $setting) {
      $form['header_list'][$id]['extension'] = [
        '#markup' => '<span>' . $setting['extension'] . '</span>',
      ];

      $form['header_list'][$id]['name'] = [
        '#markup' => '<span>' . $setting['name'] . '.' . $setting['extension'] . '</span>',
      ];

      $form['header_list'][$id]['edit'] = [
        '#type' => 'link',
        '#title' => $this->t('Edit'),
        '#attributes' => ['class' => ['button']],
        '#url' => Url::fromRoute(
          'download_otf.settings_new',
          ['id' => $id]
        ),
      ];

      $form['header_list'][$id]['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete'),
        '#attributes' => ['class' => ['button', 'button--danger']],
        '#url' => Url::fromRoute(
          'download_otf.settings_delete',
          ['id' => $id]
        ),
      ];
    }

    $form['actions']['add'] = [
      '#type' => 'link',
      '#title' => $this->t('Add New'),
      '#attributes' => ['class' => ['button', 'button--primary']],
      '#url' => Url::fromRoute('download_otf.settings_new', ['id' => 'extension']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}

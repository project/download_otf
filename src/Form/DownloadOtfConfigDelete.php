<?php

namespace Drupal\download_otf\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a confirmation form to confirm deletion of file extension settings.
 */
class DownloadOtfConfigDelete extends ConfirmFormBase {

  /**
   * Configured settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_otf_settings_delete';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $id
   *   File extension setting id.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->settings = $this->config('download_otf.settings')->get($id);

    // Redirect to create new page if improper id.
    if (!$this->settings) {
      return new RedirectResponse(Url::fromRoute('download_otf.settings')->toString());
    }

    $headers = [];
    foreach ($this->settings['headers'] as $item) {
      $headers[] = $item['header'];
    }

    $form['headers'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Configured headers will also get deleted:'),
      '#items' => $headers,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('download_otf.settings');
    $settings = $config->get();

    // Unset particular file extension item.
    unset($settings[$this->settings['id']]);
    $config->setData($settings)->save();

    // Log message.
    $msg = $this->t(
      'Configurations for file extension %extension deleted successfully.',
      ['%extension' => $this->settings['extension']]
    );
    $this->messenger()->addStatus($msg);
    $this->logger('download_otf')->notice($msg);

    $form_state->setRedirect('download_otf.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete the settings %id ?',
      ['%id' => $this->settings['extension']]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('download_otf.settings');
  }

}

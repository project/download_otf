<?php

namespace Drupal\download_otf\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines form to create new or update exisitng file extension configurations.
 */
class DownloadOtfConfigNew extends FormBase {

  /**
   * Flag identifiying an 'Add New' request.
   *
   * @var bool
   */
  protected $isNew = TRUE;

  /**
   * The date-time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $dateTime;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The link generator service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * Constructs a new DownloadOtf object.
   *
   * @param \Drupal\Component\Datetime\Time $date_time
   *   The date-time service to determine current timestamp.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to determine query parameters.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator service to create anchor links.
   */
  public function __construct(Time $date_time, RequestStack $request_stack, LinkGeneratorInterface $link_generator) {
    $this->dateTime = $date_time;
    $this->requestStack = $request_stack;
    $this->linkGenerator = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('link_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_otf_settings_new';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $id
   *   File extension setting id.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $query = $this->requestStack->getCurrentRequest()->query;
    $config = $this->configFactory()->get('download_otf.settings');
    $settings = $config->get($id);

    // Redirect to 'Add New' settings page when accessed with improper id.
    if (!$settings && $id !== 'extension') {
      return new RedirectResponse(
        Url::fromRoute('download_otf.settings_new', ['id' => 'extension'])->toString()
      );
    }

    $form['extension'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File Extension'),
      '#default_value' => $query->get('extension') ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Provide target file extension. Example csv, ics, pdf, txt, xls, xlsx etc.'),
    ];
    if ($id !== 'extension') {
      $this->isNew = FALSE;
      $form['extension']['#default_value'] = $settings['extension'];
      $form['extension']['#disabled'] = TRUE;
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Filename'),
      '#default_value' => $settings['name'] ?? 'Download',
      '#required' => TRUE,
      '#description' => $this->t('Provide default file name for downloadables. Example Export_File, Download File etc.'),
    ];

    // Existing header list.
    if (isset($settings['headers']) && !empty($settings['headers'])) {
      $form['header_list'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Header'),
          $this->t('Value'),
          $this->t('Remove'),
        ],
        '#caption' => $this->t('Below headers were already configured for downloadables:'),
      ];

      if (!empty($settings['headers'])) {
        // Existing header list items.
        foreach ($settings['headers'] as $key => $item) {
          $form['header_list'][$key]['disable'] = [
            '#markup' => '<span>' . $item['header'] . '</span>',
          ];

          $form['header_list'][$key]['enable'] = [
            '#markup' => '<span>' . $item['value'] . '</span>',
          ];

          $form['header_list'][$key]['remove'] = [
            '#type' => 'submit',
            '#name' => $key,
            '#value' => $this->t('Remove'),
            '#submit' => [[$this, 'removeIndividualHeader']],
            '#button_type' => 'danger',
          ];
        }
      }
    }

    // Add new header section.
    $form['header'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add New Header'),
    ];
    $form['header']['new_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Header'),
      '#default_value' => $query->get('header') ?? '',
    ];
    $form['header']['new_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $query->get('value') ?? '',
    ];

    $markup = $this->t('Add valid @headers for downloadables.',
      [
        '@headers' => $this->linkGenerator->generate(
          'HTTP headers',
          Url::fromUri('https://en.wikipedia.org/wiki/List_of_HTTP_header_fields', [
            'attributes' => ['target' => '_blank'],
            'fragment' => 'Standard_request_fields',
          ])
        ),
      ]
    );
    $form['header']['example'] = ['#markup' => '<span>' . $markup . '</span>'];

    // Submit action items.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
      '#name' => 'submit',
    ];
    if ($id !== 'extension') {
      $form['actions']['delete'] = [
        '#type' => 'link',
        '#title' => $this->t('Delete Configuration'),
        '#attributes' => ['class' => ['button', 'button--danger']],
        '#url' => Url::fromRoute(
          'download_otf.settings_delete',
          ['id' => $id]
        ),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $submitted = $form_state->getValues();

    // Valid file extension.
    if (!preg_match('/^(?<!\.)[a-z0-9\.]+(?<!\.)$/', $submitted['extension'], $match)
      || $submitted['extension'] === 'extension') {
      $form_state->setErrorByName('extension', $this->t('Invalid extension'));
    }

    // When value is passed without header.
    if ($submitted['new_value'] && !$submitted['new_header']) {
      $form_state->setErrorByName('new_header', $this->t('Required a valid header.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $submitted = $form_state->getValues();

    // Since, DOTs are not allowed as top-level config-key.
    // Replacing DOT with Underscore for identification.
    $id = str_replace('.', '_', $submitted['extension']);

    // Existing settings.
    $config = $this->configFactory()->getEditable('download_otf.settings');
    $settings = $config->get($id) ?? [];

    // For 'Add New' request if settings already exits; redirect to that page.
    if ($this->isNew && !empty($settings)) {
      $msg = $this->t(
        'Configurations for file extension %extension already exists here.',
        ['%extension' => $submitted['extension']]
      );
      $this->messenger()->addWarning($msg);

      $form_state->setRedirect('download_otf.settings_new', ['id' => $id]);
      return;
    }

    // Process new header-value submission.
    $new_headers = [];
    $existing_headers = $settings['headers'] ?? [];
    if ($new_header = trim($submitted['new_header'])) {
      // Unset header-value from existing header list.
      foreach ($existing_headers as $key => $item) {
        if ($item['header'] == $new_header) {
          unset($existing_headers[$key]);
          break;
        }
      }

      $new_headers = [
        'H' . $this->dateTime->getRequestTime() => [
          'header' => $new_header,
          'value' => trim($submitted['new_value']),
        ],
      ];
    }

    // Save settings.
    $config->set(
      $id,
      [
        'id' => $id,
        'name' => trim($submitted['name']),
        'extension' => $submitted['extension'],
        'headers' => array_merge($existing_headers, $new_headers),
      ]
    )->save();

    // Log message.
    if ($this->isNew) {
      $msg = $this->t(
        'Configurations for a new file extension %extension added successfully.',
        ['%extension' => $submitted['extension']]
      );
      $this->messenger()->addStatus($msg);
      $this->logger('download_otf')->notice($msg);
    }
    else {
      $this->messenger()->addStatus($this->t('Configurations updated successfully.'));
    }

    $form_state->setRedirect('download_otf.settings_new', ['id' => $id]);
  }

  /**
   * Removes individual configured header.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeIndividualHeader(array &$form, FormStateInterface $form_state) {
    $extension = $form_state->getValue('extension');

    $trigged_element = $form_state->getTriggeringElement();
    $config = $this->configFactory()->getEditable('download_otf.settings');
    $existing_config = $config->get($extension);

    // Unset particular header.
    unset($existing_config['headers'][$trigged_element['#name']]);
    $config->set($extension, $existing_config)->save();
  }

}

<?php

namespace Drupal\download_otf;

use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines Download On-The-Fly service.
 */
class DownloadOtf {

  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The link generator service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * Constructs a new DownloadOtf object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, TranslationInterface $string_translation, LinkGeneratorInterface $link_generator) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->linkGenerator = $link_generator;
  }

  /**
   * Generate download response.
   *
   * @param mixed $data
   *   File data that can be cast to string.
   * @param string $extension
   *   File extension.
   * @param string $name
   *   File name.
   * @param array $tokens
   *   An array of token-replace pair values to manage dynamic header values.
   *   Check module's README.md to for examples.
   */
  public function download($data, $extension, $name = NULL, array $tokens = []) {
    $id = str_replace('.', '_', $extension);
    $settings = $this->configFactory->get('download_otf.settings')->get();

    // Return if file extension not configured.
    if (!isset($settings[$id])) {
      $config_link = $this->linkGenerator->generate('here', Url::fromRoute(
          'download_otf.settings_new',
          ['id' => 'extension'],
          ['query' => ['extension' => $id]]
          )
        );

      $this->messenger->addError($this->t('Attempt to download unsupported file extension %extention. Configure @here.', [
        '%extention' => $extension,
        '@here' => $config_link,
      ]));

      $response = new RedirectResponse(Url::fromRoute('<front>')->toString());
      $response->send();
      return;
    }

    // Generate response for given data file.
    $response = new Response($data, 200);

    // Set configured headers.
    foreach ($settings[$id]['headers'] as $item) {
      if ($item['value']) {
        foreach ($tokens as $token => $replace) {
          $item['value'] = str_replace($token, $replace, $item['value']);
        }
      }
      $response->headers->set($item['header'], $item['value']);
    }

    // Forcefully override Content-Length, Content-Disposition header values.
    $name = ($name ?? $settings[$id]['name']) . '.' . $extension;
    $response->headers->set('Content-Length', strlen($data));
    $response->headers->set('Content-Disposition', 'attachment; filename=' . $name);

    $response->send();
  }

}

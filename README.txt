CONTENTS OF THIS FILE
---------------------
- Introduction
- Installation
- Configuration
- Permissions
- Usage


INTRODUCTION
------------
Download On-The-Fly module provides service to download files.
Files are created and downloaded dynamically without saving it to the server.

It supports all file extension whose generated data can be casted to a string.
Example file extensions csv, ics, pdf, txt, xls, xlsx etc.

Here we configure each file extension along with supporting HTTP Headers
to support downloading them On-The-Fly.

External libraries can also be used for the generation of file data.

Keep checking module page https://www.drupal.org/project/download_otf
for supporting examples, usage, guidelines etc.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8


CONFIGURATION
-------------
To configure, find 'Configure' link at admin/modules page against
the module name, OR at admin/config page find item link 'Download On-The-Fly'.

IMPORTANT:
If you want to provide downloadables On-The-Fly, say,
a QuarterReport.xls file AND/OR AnnualReport.xls file.
Then, you will NOT be configuring this module for each of the above files,
but configurations will be made only for its file extension i.e., 'xls'.

Now, say to configure 'xls' file extension, go to configuration page:
- Click on 'Add New' button
- Provide File Extension: xls
- Provide Default Filename: Download
- Under 'Add New Header' section,
  - Provide Header: Content-Type
  - Provide Value: application/vnd.ms-excel; charset=utf-8
  - Click Submit
- Again, under 'Add New Header' section,
  - Provide Header: Cache-Control
  - Provide Value: no-cache
  - Click Submit

That's it! You are done with the configuration part.
Note: Above configurations can be updated/removed at any point of time.


PERMISSIONS
-------------
Control users by
configuring permissions at admin/people/permissions#module-download_otf.

Available user permissions:
- Download On-The-Fly configuration access


USAGE
-------------
Make downloadables available On-The-Fly for the end users
by simply calling a function from any route through service 'download_otf.download'.

Examples:
(1) Using default file name (the one you configured).
$data = "Item, Price\n";
$data .= "Item1, ₹21\n";
$data .= "Item1, ₹551\n";
\Drupal::service('download_otf.download')->download($data, 'xls');

(2) With file name of your choice.
$filename = 'AnnualReport.xls';
\Drupal::service('download_otf.download')->download($data, 'xls', $filename);

(3) Supporting dynamic header values for same extension.
This will be the case when you want your header values to be dynamic.

Say, for the file extension 'xls',
- At one place your downloadable shall support 'Cache-Control: no-cache'
- At second place your downloadable shall support 'Cache-Control: private'

So, for above to work, edit 'xls' configuration on configuration page.
Now, update Cache-Control header under 'Add New Header' section:
  - Provide Header: Cache-Control
  - Provide Value: @mytoken
  - Click Submit

Now, to support above cases, call service with tokens:

[Case-1]
$tokens = [
  '@mytoken' => 'no-cache'
];
\Drupal::service('download_otf.download')->download($data, 'xls', NULL, $tokens);

[Case-2]
$tokens = [
  '@mytoken' => 'private'
];
\Drupal::service('download_otf.download')->download($data, 'xls', NULL, $tokens);
